#include <stdio.h>

int main(){

    int A[10][10],B[10][10],sum[10][10]={0},mul[10][10]={0};

    int rows1,col1,rows2,col2;

    printf("Enter the number of rows and columns in matrix A\n");
    scanf("%d%d", &rows1,&col1);

    printf("Enter the number of rows and columns in matrix B\n");
    scanf("%d%d", &rows2,&col2);

    if(rows1!=rows2 || col1!=col2){
        printf("Addition impossible.\n");

    }
    else if(col1!=rows2)
    {
       printf("Multiplication impossible.\n");
    }
    else
    {
        printf("Enter elements for the matrix A\n");
        for(int i=0;i<rows1;i++){
            for(int j=0;j<col1;j++){
                scanf("%d", &A[i][j]);
            }
        }

        printf("Enter elements for the matrix B\n");
        for(int i=0;i<rows2;i++){
            for(int j=0;j<col2;j++){
                scanf("%d", &B[i][j]);
            }

        }


        //addition
        for(int i=0;i<rows1;i++){
            for(int j=0;j<col1;j++){
                sum[i][j] = A[i][j]+ B[i][j];
            }
        }
        for (int i=0;i<rows1;i++){
            for(int j=0;j<col2;j++){
                printf("Sum is %d",sum[i][j]);
            }
            printf("/n");
        }

        //multiplication
        for(int i=0;i<rows1;i++){
            for(int j=0;j<col2;j++){
                for(int k=0;k<rows2;k++){
                    mul[i][j]+=A[i][k]*B[k][j];
                }
            }
        }

        for(int i=0;i<rows1;i++){
            for(int j=0;j<col2;j++){
                printf("Multiplication is %d",mul[i][j]);
            }

            printf("\n");
        }

    }

    return 0;
}
