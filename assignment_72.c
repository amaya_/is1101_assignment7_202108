#include <stdio.h>

int main()
{
    char string[50],c;
    int count=0;


    printf("Enter a string\n");
    fgets(string, sizeof(string), stdin);

    printf("Enter the character you need the frequency: ");
    scanf("%c", &c);

    for(int i=0; string[i]!='\0'; i++){
        if(c==string[i])
            count++;
    }

    printf("Frequency of %c is %d", c,count);

    return 0;
}
